<?php
    defined('C5_EXECUTE') or die(_("Access Denied."));

    class Recaptcha {

        const url = 'https://www.google.com/recaptcha/api/siteverify';

        public static function doRecaptchaCheck($recaptchaResponse) {
            if (defined('RECAPTCHA_SECRET_KEY') && defined('RECAPTCHA_SITE_KEY')) {
                $recaptchaPost = ['secret' => RECAPTCHA_SECRET_KEY, 'response' => $recaptchaResponse];

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => http_build_query($recaptchaPost),
                    CURLOPT_HTTPHEADER => array('Content-Type: application/x-www-form-urlencoded'),
                    CURLINFO_HEADER_OUT => false,
                    CURLOPT_HEADER => false,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYPEER => true,
                    CURLOPT_URL => self::url,
                ));
                $data = curl_exec($curl);
                curl_close($curl);

                return json_decode($data);

            } else {
                return false;
                
            }
        }

    }