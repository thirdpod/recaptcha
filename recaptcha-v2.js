var Recaptcha = function () {
    var _this = this;
    this.invisible = true;
    this.siteKey = document.getElementById('recaptcha-site-key').value;
    this.currentForm = {};
    this.forms = document.querySelectorAll('form');

    for (var idx in this.forms) {
        if (this.forms[idx] instanceof HTMLElement) {
            var form = this.forms[idx]
            var classes = Array.prototype.slice.call(form.classList);
            if (classes.indexOf('no-recaptcha') > -1) {
                delete this.forms[idx];
            }
        } else {
            delete this.forms[idx];
        }
    }

    this.prepareForms();
    this.addEventListeners();
}

Recaptcha.prototype.prepareForms = function () {
    var _this = this;
    var recaptchaElement;

    if (this.invisible) {

        recaptchaElement = document.createElement('div');
        recaptchaElement.setAttribute('id', 'invisible-recaptcha');

        document.body.appendChild(recaptchaElement);

        grecaptcha.render(recaptchaElement, {
            sitekey: _this.siteKey,
            callback: _this.submitForm.bind(_this),
            size: 'invisible',
        })

    } else {

        for (var idx in this.forms) {

            if (this.forms[idx] instanceof HTMLElement) {
                var form = this.forms[idx];
                recaptchaElement = document.createElement('div');
                recaptchaElement.setAttribute('id', 'recaptcha-' + idx);
                form.appendChild(recaptchaElement);

                grecaptcha.render(recaptchaElement, {
                    sitekey: _this.siteKey,
                    size: 'normal',
                    callback: _this.submitForm.bind(_this),
                });
            }

        }

    }
};

Recaptcha.prototype.addEventListeners = function () {
    var _this = this;

    for (var idx in this.forms) {

        if (this.forms[idx] instanceof HTMLElement) {
            var form = this.forms[idx];

            form.addEventListener('submit', function (event) {
                event.preventDefault();
                _this.currentForm = event.target;

                if (_this.invisible) {
                    grecaptcha.execute();
                } else {
                    _this.submitForm();
                }
            });
            
        }

    }
};

Recaptcha.prototype.submitForm = function () {
    var _this = this;
    var hiddenInput = document.createElement('input');

    hiddenInput.type = 'hidden';
    hiddenInput.name = 'g-recaptcha-response';
    hiddenInput.value = grecaptcha.getResponse();
    this.currentForm.appendChild(hiddenInput);


    //Simple function to submit the form. Done this way in case switch to XHR Request.
    //If switching to XHR Request, change method below.
    this.currentForm.submit();
}


function recaptchaLoadCallback() {
    new Recaptcha();
};

document.addEventListener('DOMContentLoaded', function(event) {
    if (!!document.querySelector('form') && !!document.querySelector('#recaptcha-site-key')) {
        var scriptTag = document.createElement('script');
        scriptTag.src = 'https://www.google.com/recaptcha/api.js?onload=recaptchaLoadCallback&render=explicit';
        scriptTag.async = true;
        scriptTag.defer = true;
        document.body.appendChild(scriptTag);
    }
});